"use strict";

const dotenv = require("dotenv");
dotenv.config();
const debug = require('debug')('app:jwt');
const config = require('config');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const TokenService = require('../services/token');
const tokenService = new TokenService();

// router.post('/get', tokenService.makeToken.bind(tokenService));
// router.post('/get', (req, res) => { // works in any browswer
//   tokenService.makeToken(req, res);
// }); 
router.post('/get', (req, res) => tokenService.makeToken(req, res)); // works only in es6

router.post('/verify', (req, res) => tokenService.verifyToken(req, res));

router.post('/refresh', (req, res) => tokenService.refreshToken(req, res));


module.exports = router;