'use strict';

// including modules
const dotenv = require("dotenv");
dotenv.config();
const helmet = require('helmet');
const morgan = require('morgan');
const debug = require('debug')('app:startup');
const config = require('config');
const express = require('express');
const cors = require('cors');
const home = require('./routes/index');
const token = require('./routes/token');
const reqValidator = require('./middleware/validator');

const Logger = require('./middleware/logger');
const logger = new Logger();

const PORT = process.env.PORT || 8080;

const app = express();

if(!config.get('Jwt.key')){
    console.log('FATAL ERROR: JWT secret key is not defined!')
    process.exit(1);
}

app.use(express.json());
app.use(express.urlencoded({ extended: true}));
app.use(cors());
app.use(helmet());
app.use(reqValidator);

if(app.get('env') === 'development' ){
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
    debug('morgan enabled ...')
}


// routes
app.use('/', home);
app.use('/api/jwt', token);


app.listen(PORT, () => {
    debug(`Starting app on port: ${PORT}`);
});
