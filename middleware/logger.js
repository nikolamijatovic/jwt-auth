"use strict";

const EventEmitter = require('events');

/**
 * Logger middleware that uses console.log for loging purpose.
 * this is just for playground debug and practice.
 */
class Logger extends EventEmitter{

    log(message){
        console.log(message);    
   }

}

module.exports = Logger;