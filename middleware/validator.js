"use strict";

const Logger = require('./logger');
const logger = new Logger();
const Joi = require('@hapi/joi');
/**
 * Validates if Authorization header exists in request, every request
 * must have authorization header.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
function validate(req, res, next){
      let bearer = req.headers.authorization;
      if(bearer === undefined){
         return res.status(401).send('You shell not pass!!!');
      }
      let [, token] = bearer.split(' ');

     const data = {
        authorization : token
     }; 

     const schema = Joi.object({
        authorization: Joi.string().min(5).required()
    });

     const { error, value } = schema.validate(data);

     if(error){
      logger.log(error);
      res.status(400).send({ error: error.details });
      return;        
    }

    next();
}

module.exports = validate;