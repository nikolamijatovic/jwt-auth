"use strict";

const debug = require('debug')('app:jwt');
const config = require('config');
const jwt = require('jsonwebtoken');

  let currentTime = Math.floor(Date.now()/1000);
  let secretKey = config.get('Jwt.key');

  const header = {
    "algorithm": "HS256",
    "expiresIn":  config.get('Jwt.expirySeconds'),
    "issuer": "myAwesomeDomainName", 
  };
  const payload = {};
  
/**
 * Token service
 */
class TokenService {

  /**
   * Represents TokenService
   * @constructor
   */
  constructor(){}

  fetchToken(header) {
    // let bearer = req.headers.authorization;
    // let [, token] = bearer.split(' ');
    let [, token] = header.split(' ');
      return token;
  }

  /**
   * 
   * @param {*} req 
   * @param {*} res 
   */
  makeToken(req, res) {
    let hash = this.fetchToken(req.headers.authorization);

    try{
      let client = config.get(`clients.${hash}`);
      payload.clientName = client;
    
      let token = jwt.sign(payload, secretKey, header);
      debug('token: ' + token);
      
      res.send({"token": token});

    } catch(err){
      res.status(400).send({'message':'Invalid authorization token!'});
    }
  }

  /**
   * Validation of token, validate signiture and expiration time
   * @param {*} req 
   * @param {*} res 
   */
  verifyToken(req, res) {
    let token = this.fetchToken(req.headers.authorization);
  
    try{
      let payload = jwt.verify(token, secretKey, {} );
      res.send(payload);
    } catch(err){
      debug(err);
      res.status(401).send({
        "message": "Invalid token!"
      });
    }
  }

  /**
   * 
   * @param {*} req 
   * @param {*} res 
   */
  refreshToken(req, res) {
    let token = this.fetchToken(req.headers.authorization);

    try{
      //verify signiture and ingore expiration of token
      let tokenDecoded = jwt.verify(token, secretKey, { ignoreExpiration: true });    
      let newPayload = {
        "clientName": tokenDecoded.clientName
      };
    
      let newToken = jwt.sign(newPayload, secretKey, header);
      debug('new token issued: ' + newToken);
      res.send({"token": newToken});
  
    } catch(err) {
      debug(err);
      res.sendStatus(401);  
    }
  }
}

module.exports = TokenService;