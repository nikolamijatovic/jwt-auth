# README #



### What is this repository for? ###
Simple JWT Auth service writen in Node.JS 
Client app can use this service to authorize access to other web services.

### Requirements
* [Docker](https://www.docker.com/)

### How do I get set up? ###

#### Configuration
    1. copy .env_example file as .env
    2. set env vars in .env file
    
#### Start app
    $ docker-compose up --build

